CREATE DATABASE MyPushNotification;

Go

create table [dbo].Contacts(
	ContactId int identity not null primary key,
	ContactName varchar(100) not null,
	ContactNo varchar(50) not null,
	AddedOn Datetime not null
)

Go

ALTER DATABASE MyPushNotification SET ENABLE_BROKER;
go

insert into Contacts (ContactName,ContactNo,AddedOn)
values ('Tung','0123445',GETDATE())