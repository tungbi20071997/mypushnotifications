namespace RealtimeNotiffication
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class MyPushNotificationModel : DbContext
    {
        public MyPushNotificationModel()
            : base("name=MyPushNotificationModelEntity")
        {
        }

        public virtual DbSet<Contact> Contacts { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Contact>()
                .Property(e => e.ContactName)
                .IsUnicode(false);

            modelBuilder.Entity<Contact>()
                .Property(e => e.ContactNo)
                .IsUnicode(false);
        }
    }
}
